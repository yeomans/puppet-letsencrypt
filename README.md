# Puppet-Letsencrypt

Puppet-Letsencrypt is a Puppet module for managing Letsencrypt SSL certificates.
It is tested with Debian Jessie and Puppet 4.  It might work with Puppet 3 + future parser.


## class letsencrypt:

Manages letsencrypt.

### Parameters

`$ssl_certs` : hash of letsencrypt::ssl_cert resource data.
`$generate_dhparam` : if true, a Diffie-Hellman parameters file is 
generated.  letsencrypt::dhparam default parameters can be overridden in 
hiera.  Default value is true.
`$src_dir='/usr/local/src': the directory in which the letsencrypt source directory should be created.
It is passed to letsencrypt::package and letsencrypt::ssl_certs resources.
`$conf_dir='/etc/letsencrypt'`: The directory in which letsencrypt stores configuration 
data and ssl certificates.


## class letsencrypt::package

Installs the Letsencrypt package from its github repository. This class requires git to be 
installed, and so realizes a virtual package resource.

### Parameters

You typically would not need to supply any of these.

`$src_dir='/usr/local/src'`  
`$src_dir_owner='root'`
`$src_dir_group='staff'`
`$src_dir_mode='2775'`
`$conf_dir='/etc/letsencrypt'`

## define letsencrypt::ssl_certs

A resource representing an SSL certificate.  

### Parameters

`$title`: the domain name.
`$email`: email address to which letsencrypt should send notifications.
`$renew=5: days before expiration that certificate renewal should be attempted.
`$webroot_path`: webroot path for renewal.  This assumes that your server is appropriately 
configured.
`$service_name: the service to reload/restart following changes to configuration.
`$src_dir='/usr/local/src': the directory in which the letsencrypt source directory should be created.
`$conf_dir='/etc/letsencrypt'`: The directory in which letsencrypt stores configuration 
data and ssl certificates.
`$use_staging=false: if true, the --staging flag is passed to letsencrypt to use their staging server 
for certificate creation.  
`$use_self_signed_cert=false: if true, self-signed certificates are created locally instead of 
asking letsencrypt for them.  Intended for use with local development servers that won't have 
a public DNS entry.