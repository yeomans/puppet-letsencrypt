# Manages a Diffie Hellman ephemeral parameters file.
# $dhparam_path = path to the file. Default is /etc/letsencrypt/dhparam.pem.
# $size = key size.  Default is 4096.
# Note that file generation takes some time.
# $dhparam_value : the contents of a dhparam file.  Because generation takes so long, 
# it may be preferable to generate a file and add its contents to hiera.  If a value for 
# this parameter is supplied, it will be written to $dhparam_path instead of a new 
# file being generated by openssl.

class letsencrypt::dhparam($dhparam_path='/etc/letsencrypt/dhparam.pem', $size=4096, $dhparam_value=undef)
    {
    if $dhparam_value != undef
        {
        file
            {
            "$dhparam_path":
            ensure => file,
            owner => 'root', 
            group => 'root', 
            mode => '0644',
            content => "$dhparam_value",
            }
        }
    else
        {
        exec
            {
            "generate_dhparam":
            command => "openssl dhparam -out $dhparam_path $size > /dev/null",
            path => "$::path",
            creates => "$dhparam_path",
            }
        }
    }
