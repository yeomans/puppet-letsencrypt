class letsencrypt(
    Hash $ssl_certs={}, 
    Boolean $generate_dhparam=true, 
    $src_dir='/usr/local/src',
    $conf_dir='/etc/letsencrypt'
    )
    {
    class{'letsencrypt::package':  src_dir=>"$src_dir", conf_dir=>"$conf_dir"}
    create_resources(letsencrypt::ssl_cert, $ssl_certs, $defaults={'src_dir' => "$src_dir", 'conf_dir'=>"$conf_dir"})
    if $generate_dhparam
        {
        class{'letsencrypt::dhparam':}
        }
    }
