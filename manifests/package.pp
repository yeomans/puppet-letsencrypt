class letsencrypt::package
    (
    $src_dir='/usr/local/src', 
    $src_dir_owner='root', 
    $src_dir_group='staff',
    $src_dir_mode='2775',
    $conf_dir='/etc/letsencrypt'
    )
    {
    realize Package['git']
    
    file
        {
        "$src_dir":
        ensure => directory, 
        owner => $src_dir_owner, 
        group => $src_dir_group,
        mode => $src_dir_mode,
        }

   exec
        { 
        "git clone https://github.com/letsencrypt/letsencrypt \"$src_dir/letsencrypt\"":
        creates => "$src_dir/letsencrypt",
        path => "$path",
        }  
        
    file
        {
        '/usr/local/bin/letsencrypt-renew':
        ensure => present,
        owner => 'root',
        group => 'root', 
        mode => '0744',
        source => "puppet:///modules/letsencrypt/letsencrypt-renew"
        }

    # These directories are not created by letsencrypt until it is run.  But we need them them 
    # to create a dhparam file, and to create self-signed ssl certificates if requested.        
    file
        {
        "$conf_dir":
        ensure => directory, 
        owner => 'root', 
        group => 'root', 
        mode => '0755',
        }
        
        file
            {
            "$conf_dir/archive":
            ensure => directory,
            owner => 'root',
            group => 'root',
            mode => '0755',
            }
            
        file
            {
            "$conf_dir/live":
            ensure => directory,
            owner => 'root',
            group => 'root',
            mode => '0755',
            }
    }
