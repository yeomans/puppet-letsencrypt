define letsencrypt::ssl_cert
    (
    $email, 
    $renew=5,
    $webroot_path,
    $service_name,
    $src_dir='/usr/local/src',
    $conf_dir='/etc/letsencrypt',
    $use_staging=false,
    $use_self_signed_cert=false,
    )
    {

    $live_dir = "$conf_dir/live/$title"   
    if $use_self_signed_cert
        {
        $archive_dir = "$conf_dir/archive/$title"
          
        file
            {
            "$live_dir":
            ensure => directory,
            owner => 'root',
            group => 'root',
            mode => '0755',
            }
            
        file
            {
            "$archive_dir":
            ensure => directory,
            owner => 'root',
            group => 'root',
            mode => '0755',
            }

        $privkey = "privkey.pem"
        $cert = "cert.pem"
        $chain = "chain.pem"
        $fullchain = "fullchain.pem"
        
        exec
            {
            "generate_letsencrypt_self_signed_cert_$title":
            command => "openssl req -x509 -nodes -newkey rsa:4096 -keyout $archive_dir/$privkey -out $archive_dir/$cert -days 30 -subj '/C=/ST=/L/O=/OU=Org/CN=$title'",
            path => "$path",
            unless => "openssl x509 -checkend 0 -noout -in $archive_dir/$cert 2> /dev/null"
            } ~>
        exec
            {
            "cp $archive_dir/$cert $archive_dir/$fullchain":
            path => "$path",
            refreshonly => true,
            }
            
        file
            {
            "$live_dir/$privkey":
            ensure => link,
            target => "$archive_dir/$privkey",
            }

        file
            {
            "$live_dir/$cert":
            ensure => link,
            target => "$archive_dir/$cert",
            }
            
        file
            {
            "$live_dir/$fullchain":
            ensure => link,
            target => "$archive_dir/$fullchain",
            }      
        }
    else
        {
        #what if $use_staging is changed in manifest? that results in a different exec resource, but same creates argument.
        
        $staging = $use_staging ? {true => ' --staging', false => ''}
        exec
            {
            "\"$src_dir/letsencrypt/letsencrypt-auto\" certonly --standalone --non-interactive --email '$email' --agree-tos -d '$title'$staging":
            creates => "$live_dir",
            }
    
        cron
            {
            "letsencrypt-renew-$title":
            ensure => present, 
            command => "/usr/local/bin/letsencrypt-renew --renew $renew --webroot-path \"$webroot_path\" --service \"$service_name\" $title",
            environment => "PATH=$path",
            hour => [fqdn_rand(12, "$title"), fqdn_rand(12, "$title") + 12],
            minute => fqdn_rand(60, "$title"),
            }
        }
    }
